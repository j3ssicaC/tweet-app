var app = new Vue({
  el: '#app',
  data: {
    showModal: false,
    tweets: [
    'This is second tweet',
    'This is first tweet',
    ],
    newTweet: '',
    maxChars: 140,
    loading: false
  },
  computed: {
    validLength(){
      return this.newTweet.length > this.maxChars
    },
    AantalChars(){
      return this.maxChars - this.newTweet.length
      
    },
    reverse(){
      return this.tweets.slice().reverse();
    }
  },
  methods: {
    addTweet(){
      this.tweets.push(this.newTweet);
      this.newTweet = '';
      this.loading = true
      
      setTimeout(() => {
        this.loading = false
        
        this.closeModal();

      },3000);
    },
    
    addModal(){
      this.showModal = true
    },
    closeModal(){
      this.showModal = false
    }
  }
});
